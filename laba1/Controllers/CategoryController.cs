﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using laba1.DataAccessLevel;
using laba1.Models;
using laba1.DataAccessLevel;

namespace laba1.Controllers
{
    public class CategoryController : Controller
    {
        CategoryDataAccess CategoryDA = new CategoryDataAccess();

        public ActionResult Index()
        {
            return View(CategoryDA.getAllTeams());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category Category = CategoryDA.getParticularCategory(id);
            if (Category == null)
            {
                return HttpNotFound();
            }
            return View(Category);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title")] Category Category) 
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CategoryDA.Add(Category);
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(Category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                CategoryDA.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
