﻿using System.Net;
using System.Web.Mvc;
using laba1.DataAccessLevel;
using laba1.Models;
using System;

namespace laba1.Controllers
{
    public class ServiceController : Controller
    {
        private SampleContext db = new SampleContext();

        ServiceDataAccess serviceDA = new ServiceDataAccess();

        public ActionResult Index()
        {
            return View(serviceDA.getAllServices());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = serviceDA.getService(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categorys, "Id", "Title");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description")] Service service)
        {
            if (ModelState.IsValid)
            {
                db.Services.Add(service);
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categorys, "Id", "Title", service.CategoryId);
            return View(service);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            ViewBag.myid = service.CategoryId;
            if (service == null)
            {
                return HttpNotFound();
                
            }
           
            ViewBag.CategoryId = new SelectList(db.Categorys, "Id", "Title", service.CategoryId);
            return View(service);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title, Description, CategoryId")] Service service)
        {
            if (ModelState.IsValid)
            {
                serviceDA.Update(service);
                return RedirectToAction("Index");
            }
            ViewBag.TeamId = new SelectList(db.Categorys, "Id", "Title", service.CategoryId);

            return View(service);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = serviceDA.getService(id);

            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            serviceDA.DeleteService(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                serviceDA.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
