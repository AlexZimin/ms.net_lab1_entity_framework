﻿using laba1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace laba1.DataAccessLevel
{
    public class ServiceDataAccess
    {
        private SampleContext db;

        public ServiceDataAccess()
        {
            db = new SampleContext();
        }

        public List<Service> getAllServices()
        {
            return db.Services.ToList();
        }

        public Service getService(int? id)
        {
            return db.Services.Find(id);
        }

        public DbSet<Category> getAllCategorys()
        {
            return db.Categorys;
        }


        public void Update(Service p)
        {
            var service = db.Services.Find(p.Id);

            service.Title = p.Title;
            service.Description = p.Description;
            service.CategoryId = p.CategoryId;

            db.SaveChanges();
        }
        

        public void Add(Service service)
        {
            db.Services.Add(service);
            db.SaveChanges();
        }

        public void DeleteService(int id)
        {
            db.Services.Remove(db.Services.Find(id));
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}