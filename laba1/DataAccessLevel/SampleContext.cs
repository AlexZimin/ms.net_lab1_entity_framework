﻿using laba1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace laba1.DataAccessLevel
{
    public class SampleContext : DbContext
    {

        public SampleContext() : base("MyCategorysAndServices")
        { }

        public DbSet<Category> Categorys { get; set; }
        public DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}