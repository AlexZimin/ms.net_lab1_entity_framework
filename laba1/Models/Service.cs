﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace laba1.Models
{
    public class Service
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}