﻿using laba1.Models;
using System.Collections.Generic;
using System.Linq;

namespace laba1.DataAccessLevel
{
    public class CategoryDataAccess
    {
        private SampleContext db;

        public CategoryDataAccess()
        {
            db = new SampleContext();
        }

        public Category getParticularCategory(int? id)
        {
            return db.Categorys.Find(id);
        }

        public List<Category> getAllTeams()
        {
            return db.Categorys.ToList();
        }

        public void Add(Category Category)
        {
            db.Categorys.Add(Category);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }

    }
}