﻿using laba1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace laba1.DataAccessLevel
{
    public class SampleInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SampleContext>
    {
        protected override void Seed(SampleContext context)
        {
            var Categorys = new List<Category>
            {
                new Category {Title = "Уголовный"},
                new Category {Title = "Административный"},
                new Category {Title = "Гражданский"},
            };

            Categorys.ForEach(c => context.Categorys.Add(c));
            context.SaveChanges();

            var services = new List<Service>
            {
                new Service{ Title = "Адвакатура", Description = "Защита интересов в суде", CategoryId = 1 },
                new Service{ Title = "Составление заключения", Description = "Недорого", CategoryId = 2 },
                new Service{ Title = "Оплата госпошлины", Description = "Бесценно", CategoryId = 3 },
            };
            services.ForEach(s => context.Services.Add(s));
            context.SaveChanges();

        }

    }
}